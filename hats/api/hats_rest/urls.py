from django.urls import path, include
from hats_rest import views


urlpatterns = [
    path("", views.hat_list, name="hat_list"),
    path("<int:id>/", views.show_hat, name="show_hat"),
]