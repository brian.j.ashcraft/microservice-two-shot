import json
from django.shortcuts import render
from .models import Hat, LocationVO
from common.json import ModelEncoder
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods


# Create your views here.

class LocationVODetailEncoder(ModelEncoder):
    model = LocationVO
    properties = ["closet_name", "import_href"]


class HatListEncoder(ModelEncoder):
    model = Hat
    properties = ["style_name", "id"]


class HatDetailEncoder(ModelEncoder):
    model = Hat
    properties = [
        "fabric",
        "style_name",
        "color",
        "picture_url",
        "location",
    ]
    encoders = {
        "location": LocationVODetailEncoder(),
    }


# @require_http_methods(["GET", "POST"])
# def api_list_hats(request):
#     if request.method == "GET":
#         hats = Hat.objects.all()
#         return JsonResponse(
#             {"hats": hats},
#             encoder=HatDetailEncoder,
#         )
#     else:
#         content = json.loads(request.body)

#         try:
#             location_href = content["location"]
#             location = LocationVO.objects.get(import_href=location_href)
#             content["location"] = location
#         except LocationVO.DoesNotExist:
#             return JsonResponse(
#                 {"message": "Invalid location"},
#                 status=400,
#             )

#         hat = Hat.objects.create(**content)
#         return JsonResponse(
#             hat,
#             encoder=HatDetailEncoder,
#             safe=False,
#         )


@require_http_methods(["GET", "POST"])
def hat_list(request):
    if request.method == "GET":
        hats = Hat.objects.all()
        return JsonResponse(
            {"hats": list(hats.values())},
            encoder=HatListEncoder,
        )
    elif request.method == "POST":
        content = json.loads(request.body)

        location_id = content.get("location")
        if location_id is None:
            return JsonResponse(
                {"message": "Invalid location ID"},
                status=400,
            )

        try:
            location = LocationVO.objects.get(id=location_id)
            content["location"] = location
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location ID"},
                status=400,
            )

        hat = Hat.objects.create(**content)
        hat_data = HatDetailEncoder().default(hat)  # Convert hat object to dictionary
        return JsonResponse(
            HatDetailEncoder().encode(hat_data),  # Updated encoding of hat object
            encoder=HatDetailEncoder,
            safe=False,
        )


@require_http_methods(["GET", "DELETE", "PUT"])
def show_hat(request, id):
    if request.method == "GET":
        try:
            hat = Hat.objects.get(id=id)
            return JsonResponse(
                HatDetailEncoder().default(hat),
                encoder=HatDetailEncoder,
                safe=False,
            )
        except Hat.DoesNotExist:
            return JsonResponse(
                {"message": "Hat not found"},
                status=404
            )
    elif request.method == "DELETE":
        try:
            hat = Hat.objects.get(id=id)
            hat.delete()
            return JsonResponse({"message": "Hat deleted successfully"})
        except Hat.DoesNotExist:
            return JsonResponse({"message": "Hat not found"}, status=404)
    elif request.method == "PUT":
        content = json.loads(request.body)
        try:
            if "location" in content:
                location = LocationVO.objects.get(import_href=content["location"])
                content["location"] = location
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location import href"},
                status=400,
            )

        try:
            hat = Hat.objects.get(id=id)
            for key, value in content.items():
                setattr(hat, key, value)
            hat.save()
            return JsonResponse(
                HatDetailEncoder().default(hat),
                encoder=HatDetailEncoder,
                safe=False,
            )
        except Hat.DoesNotExist:
            return JsonResponse(
                {"message": "Hat not found"},
                status=404
            )
