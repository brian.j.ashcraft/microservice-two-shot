# Wardrobify

Team:

* Person 1 - Brian Ashcraft 
* Person 2 - Ashur Prather

## Design

## Shoes microservice

<!-- Explain your models and integration with the wardrobe
microservice, here. -->
making a shoes model that holds traits of the shoe. There will be views that call the model that will pull a shoe with its details, pull all the shoes into a list, allow you to update traits of or delete a shoe.
## Hats microservice
<!-- Explain your models and integration with the wardrobe
microservice, here. -->
My model shall be a hat model along with the locationVO model to reference the wardrobe. The views will be made to create, list, and delete hats to and from the wardrobe. Listing will have their specific id numbers for location and placement within the wardrobe aka shelf number.