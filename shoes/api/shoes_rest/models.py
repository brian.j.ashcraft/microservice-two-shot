from django.db import models
from django.urls import reverse

# Create your models here.
class BinVO(models.Model):
    closet_name = models.CharField(max_length=100)
    bin_number = models.PositiveSmallIntegerField()
    bin_size = models.PositiveSmallIntegerField()
    import_href = models.CharField(max_length=200, unique=True)
   


class Shoe(models.Model):
    manufacturer = models.CharField(max_length=100)
    model_name = models.CharField(max_length=100)
    color = models.CharField(max_length=100)
    pic_url = models.URLField() 

    bin =  models.ForeignKey(
        "binVO",
        related_name="shoe",
        on_delete=models.CASCADE
    )
    def __str__(self):
        return self.name

    def get_api_url(self):
        return reverse("show_shoe", kwargs={"pk": self.pk})
