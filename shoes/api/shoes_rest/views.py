from django.http import JsonResponse
from django.shortcuts import render
from .models import Shoe, BinVO
import json
from common.json import ModelEncoder
from django.views.decorators.http import require_http_methods
# Create your views here.

class BinVODetailEncoder(ModelEncoder):
    model = BinVO
    properties = ["name", "import_href"]

class ShoeListEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "name",
    ]


class ShoeDetailEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "manufacturer",
        "model_name",
        "color",
        "pic_url",
    ]
    encoders = {
        "binVO": BinVODetailEncoder(),
    }

@require_http_methods(["GET", "POST"])
def api_list_shoes(request, bin_vo_id=None):
    if request.method == "GET":
        if bin_vo_id ==None:
            shoes = shoe.objects.all()
        else:
            shoes = Shoe.objects.filter(bin=bin_vo_id)
        return JsonResponse(
            {"shoes": shoes},
            encoder=ShoeListEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            bin_href = f'/api/bin/{bin_vo_id}/'
            bin = BinVO.objects.get(import_href= bin_href)
            content["bin"] = bin
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid bin id"},
                status=400,
            )

        shoe = Shoe.objects.create(**content)
        return JsonResponse(
            shoe,
            encoder=ShoeDetailEncoder,
            safe=False,
        )

# class ShoeDetail(ModelEncoder):
    

# class ShoeList(ModelEncoder):
