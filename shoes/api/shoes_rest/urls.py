from django.urls import path
from .views import api_list_shoes
# from shoes_rest import views

urlpatterns = [
    path("shoes/", api_list_shoes, name="api_list_shoes"),
    # path("<int:id>/" , views.show_shoes, name="show_shoe")

]